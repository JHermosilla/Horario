package com.example.javier.myapplication;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

public class ModalAsignatura {


    public interface FinalizarModal{
        void mostrarLabel(String nombreAsignatura, String salaAsignatura);
    }
    private FinalizarModal interfaz;

    public ModalAsignatura(Context contexto, FinalizarModal actividad){
        interfaz = actividad;
        final Dialog dialogo = new Dialog(contexto);
        dialogo.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogo.setCancelable(false);
        dialogo.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogo.setContentView(R.layout.modal_asignatura);

        final EditText nombreAsignatura = dialogo.findViewById(R.id.asignatura);
        final EditText salaAsignatura =  dialogo.findViewById(R.id.sala);
        final Button agregarAsignatura = dialogo.findViewById(R.id.btnAgregarAsignatura);

        agregarAsignatura.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                interfaz.mostrarLabel(nombreAsignatura.getText().toString() , salaAsignatura.getText().toString());
                dialogo.dismiss();
            }
        });
        dialogo.show();
    }
}
