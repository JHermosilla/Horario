package com.example.javier.myapplication;


import android.app.TimePickerDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;


public class MainActivity extends AppCompatActivity implements ModalAsignatura.FinalizarModal, View.OnClickListener {

    private Context contexto;
    private TextView horaBloque1, horaBloque2, horaBloque3, lunesBloque1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        horaBloque1 = findViewById(R.id.bloqueHora1);
        horaBloque2 = findViewById(R.id.bloqueHora2);
        horaBloque3 = findViewById(R.id.bloqueHora3);
// farfy culiao
        horaBloque1.setOnClickListener(this);
        horaBloque2.setOnClickListener(this);
        horaBloque3.setOnClickListener(this);


        contexto = this;

        lunesBloque1 =  findViewById(R.id.lunesBloque1);
        lunesBloque1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ModalAsignatura(contexto, MainActivity.this);
            }
        });

    }

    @Override
    public void onClick(View v) {
        int hora,minutos;

        if (v == horaBloque1){
            final Calendar c = Calendar.getInstance();
            hora = c.get(Calendar.HOUR_OF_DAY);
            minutos = c.get(Calendar.MINUTE);

            TimePickerDialog timerPickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    horaBloque1.setText(hourOfDay + ":" + minute);
                }
            },hora,minutos,false);
            timerPickerDialog.show();
        }

        if (v == horaBloque2){
            final Calendar c = Calendar.getInstance();
            hora = c.get(Calendar.HOUR_OF_DAY);
            minutos = c.get(Calendar.MINUTE);

            TimePickerDialog timerPickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    horaBloque2.setText(hourOfDay + ":" + minute);
                }
            },hora,minutos,false);
            timerPickerDialog.show();
        }

        if (v == horaBloque3){
            final Calendar c = Calendar.getInstance();
            hora = c.get(Calendar.HOUR_OF_DAY);
            minutos = c.get(Calendar.MINUTE);

            TimePickerDialog timerPickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    horaBloque3.setText(hourOfDay + ":" + minute);
                }
            },hora,minutos,false);
            timerPickerDialog.show();
        }
    }

    @Override
    public void mostrarLabel(String nombreAsignatura, String salaAsignatura) {
        lunesBloque1.setText(nombreAsignatura + "\n" +salaAsignatura);
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
